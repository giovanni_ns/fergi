<?php

namespace Fergi\Controllers;

use Twig\Loader\FilesystemLoader;
use Fergi\Models\Home\Home as HomeModel;
use Twig\Environment;

class Home
{
    
    static public function index()
    {
        $loader = new FilesystemLoader();
        $twig = new Environment($loader);
        $template = $twig->load('index.html.twig');
        echo $template->render( ['msg' => HomeModel::showMessage()]);
    }
}