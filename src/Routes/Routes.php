<?php

namespace Fergi\Routes;

use Bramus\Router\Router;
use Fergi\Controllers\Home;

$router = new Router();

$router->get(
    '/',
    function () {
        Home::index();
    }
);

$router->run();